def create_person():
    name = (input("Please enter name: "))
    age = int(input("Please enter age: "))
    address = (input("Please enter address: "))
    phone_number = (input("Please enter phone_number: "))
    person = Person(name, age, address, phone_number)
    return person


class Person:

    def __init__(self, name, age, address, phone_number):
        self.name = name
        self.age = age
        self.address = address
        self.phone_number = phone_number

    def display(self):
        print("Name: ", self.name, " age: ", self.age, "address: ", self.address, "phone_number: ", self.phone_number)

