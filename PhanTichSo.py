def phan_tich_so(n):
    i = 2
    listNumber = []
    while n > 1 & i < n / 2:
        if n % i == 0:
            n = int(n / i)
            listNumber.append(i)
        else:
            i = i + 1

    if len(listNumber) == 0:
        listNumber.append(n)
    return listNumber


def show():
    n = int(input("Please enter so nguyen duong n : "))
    listNumbers = phan_tich_so(n)
    size = len(listNumbers)
    sb = ""
    for i in range(0, size - 1):
        sb = sb + str(listNumbers[i]) + " x "
    sb = sb + str(listNumbers[size - 1])
    print("Kết quả:", n, "=", sb)