def bubble_sort(list_number):
    for i in range(0, len(list_number) - 1):
        for j in range(len(list_number) - 1):
            if list_number[j] > list_number[j + 1]:
                temp = list_number[j]
                list_number[j] = list_number[j + 1]
                list_number[j + 1] = temp
    return list_number


def show():
    list_number = [5, 3, 8, 6, 7, 2]
    print("The unsorted list is: ", list_number)
    print("The sorted list is: ", bubble_sort(list_number))
