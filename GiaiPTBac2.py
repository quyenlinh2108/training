import math


# giai phuong trinh bac 2
def giai_pt_bac2(a, b, c):
    if a == 0:
        if b == 0:
            print("Phuong trinh vo nghiem!")
        else:
            print("Phuong trinh co 1 nghiem x = ", float(-c / (2 * b)))
        return
    delta = b * b - 4 * a * c
    if delta > 0:
        x1 = float((-b - math.sqrt(delta)) / (2 * a))
        x2 = float((-b + math.sqrt(delta)) / (2 * a))
        print("Phuong trinh co 2 nghiem la \nx1: ", x1, "\nx2", x2)
    elif delta == 0:
        x1 = float(-b / (2 * a))
        print("Phuong trinh co nghiem kep x1 = x2 ", x1)
    else:
        print("phuong trinh vo nghiem")


def show():
    print("Giai phuong trinh bac 2")
    a = float(input(" Enter a : "))
    b = float(input(" Enter b : "))
    c = float(input(" Enter c : "))
    giai_pt_bac2(a, b, c)
